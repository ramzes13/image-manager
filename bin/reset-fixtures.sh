#!/bin/bash
#go to app directory
cd "$(dirname "$0")"/..

echo "php app/console doctrine:database:drop --if-exists  --force";
php app/console doctrine:database:drop --if-exists  --force;

echo "php app/console doctrine:database:create";
php app/console doctrine:database:create;

echo "php app/console doctrine:schema:update --force";
php app/console doctrine:schema:update --force

echo "php app/console doctrine:fixtures:load -n";
php app/console doctrine:fixtures:load -n
