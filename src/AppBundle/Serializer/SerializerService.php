<?php

namespace AppBundle\Serializer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;


class SerializerService
{
    /**
     * @param $objectToSerialize
     * @param array $groups
     * @return array
     */
    public function serialize($objectToSerialize, $groups)
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $encoders = new JsonEncoder();

        $serializer = new Serializer([$normalizer], [$encoders]);


        return $serializer->serialize($objectToSerialize, 'json', ['groups' => $groups]);
    }
}