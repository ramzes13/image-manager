<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    protected function createApiResponse($data, $statusCode = 200)
    {
        return new Response($data, $statusCode, array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @Route("/", name="homepage")
     * @Template
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $serializer = $this->get('app.serializer');

        $albumRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Album');
        $album = $albumRepository->find(1);

        $response = $serializer->serialize($album, ['default']);

//        return $this->createApiResponse($response);
        return array();
    }
}
