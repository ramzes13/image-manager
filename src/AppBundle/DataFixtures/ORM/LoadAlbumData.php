<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAlbumData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $album = new Album();
        $album->setName('image name');
        $album->setDescription('some description');

        $this->addReference('album-1', $album);
        
        $manager->persist($album);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }


}