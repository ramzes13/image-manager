<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Image;

class LoadImageData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0 ; $i < 5; $i++) {
            $image = new Image();
            $image->setName('image name' . $i);
            $image->setDescription('some description' . $i);

            $image->setAlbum($this->getReference('album-1'));

            $manager->persist($image);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }


}